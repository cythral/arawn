const 
    Channel = require("./Channel.js"),
    Member = require('./Member.js')
    Perms = require('../../permissions.json');

// symbols
const 
    $setupChannels = Symbol(),
    $setupRoles = Symbol(),
    $fetchBans = Symbol(),
    $setupMembers = Symbol();


module.exports = class Guild {
    constructor({ data, mgr }) {

        this.mgr = mgr;
        this[$setupRoles](data.roles);
        delete data.roles;

        this[$setupChannels](data.channels);
        delete data.channels;
        console.log(this.channels);

        this[$setupMembers](data.members);
        delete data.members;

        for(let key in data) this[key] = data[key];
        
        
        if(this.me.hasPerm(Perms.BAN_USERS)) this[$fetchBans]();
        else this.bans = {};
    }

    [$setupChannels](channels) {
        this.channels = {};
        for(let channel of channels) this.channels[channel.id] = new Channel({ data : channel, guild : this, mgr : this.mgr });
    }

    [$setupRoles](roles) {
        this.roles = {};
        for(let role of roles) this.roles[role.id] = role;
    }

    [$setupMembers](members) {
        this.members = {};
        for(let member of members) this.members[member.user.id] = new Member({ data : member, guild : this, mgr : this.mgr });
        this.me = this.members[this.mgr.id];
    }

    async [$fetchBans]() {
        try {
            let bans = await this.mgr.queue.add({
                endpoint : `https://discordapp.com/api/guilds/${this.id}/bans`,
                method : "GET"
            });

            this.bans = {};
            for(let ban of bans) {
                this.bans[ban.user.id] = ban;
            }
        } catch(err) {
            console.log("failed to fetch bans");
        }
    }

    

    createChannel(data) {
    	return this.mgr.queue.add({
    		endpoint : `https://discordapp.com/api/guilds/${this.id}/channels`,
    		data : data,
    		method : "POST"
    	});
    }

    ban(id, reason = null, days = 0) {
    	return this.mgr.queue.add({
    		endpoint : `https://discordapp.com/api/guilds/${this.id}/bans/${id}`,
    		qs : { "delete-message-days" : days, reason : reason },
    		method : "PUT"
    	})
    }

    unban(id) {
    	return this.mgr.queue.add({
    		endpoint : `https://discordapp.com/api/guilds/${this.id}/bans/${id}`,
    		method : "DELETE"
    	});
    }

    kick(id) {
        return this.mgr.queue.add({
            endpoint : `https://discordapp.com/api/guilds/${this.id}/members/${id}`,
            method : "DELETE"
        });
    }



};

