// Imports
const 
    EventEmitter = require('events').EventEmitter,
    Request = require('request-promise-native');

module.exports = class EndpointQueue {
    constructor(token, useragent) {
        this.token = token;
        this.useragent = useragent;
        this.queue = [];
        this.endpoints = {};
    }
    
    add(task) {
        this.createEndpoint(task.endpoint);
        task.router = new EventEmitter;
        this.queue.push(task);
   
        if(!this.endpoints[task.endpoint].limited && !this.endpoints[task.endpoint].onjob) this.loop();

        return new Promise((resolve, reject) => {
            task.router.on("done", (data) => {
                resolve(data);
            });
        });
    }
    
    loop() {
        if(this.queue.length > 0) {

            // if on job for endpoint, move to back of queue
            if(this.endpoints[this.queue[0].endpoint].onjob) {
                this.queue.push(this.queue.shift());
                return;
            }


            let task = this.queue.shift();
            this.endpoints[task.endpoint].onjob = true;

            if(this.endpoints[task.endpoint].limited) {
                this.queue.push(task);
                this.endpoints[task.endpoint].onjob = false;
                return;
            }

           this.makeRequest(task);          
        }

    }
    
    
    createEndpoint(endpoint) {
        if(!Object.keys(this.endpoints).includes(endpoint)) {
            this.endpoints[endpoint] = {};
            this.endpoints[endpoint].onjob = false;
            this.endpoints[endpoint].remaining = 5;
            this.endpoints[endpoint].limit = 5;
            this.endpoints[endpoint].resetTime = (new Date).getTime();
        }
    }
    
    
    async makeRequest({ endpoint, method, data, router, qs }) {
        var resp = await Request({
            uri : endpoint,
            method : method,
            body : data,
            json : true,
            resolveWithFullResponse: true,
            simple : false,
            qs : qs,
            headers : {
                "Authorization" : this.token,
                "User-Agent" : this.useragent
            }
        });

        this.endpoints[endpoint].limit = parseInt(resp.headers["x-ratelimit-limit"]);
        this.endpoints[endpoint].remaining = parseInt(resp.headers["x-ratelimit-remaining"]);
        this.endpoints[endpoint].resetTime = parseInt(resp.headers["x-ratelimit-reset"]);
        
        if(this.endpoints[endpoint].remaining === 0) {
            this.endpoints[endpoint].limited = true;
            this.endpoints[endpoint].retryAfter = (this.endpoints[endpoint].resetTime - ((new Date).getTime() / 1000)) * 1000;
        }    

        // endpoint rate limited, put back in the queue
        if(resp.statusCode === 429) {
            console.log("429");
            if(!resp.body.global) {
                this.endpoints[endpoint].limited = true;
                this.endpoints[endpoint].retryAfter = resp.body["retry_after"];
            } else {
                console.log("global");
                this.tries = 0;
                this.limited = true;
                this.retryAfter = resp.body["retry_after"];
            }
            
            this.queue.unshift({ endpoint, method, data, router, qs });
        }
            
        if(this.endpoints[endpoint].limited) {

            setTimeout(() => {
                this.endpoints[endpoint].limited = false;
                this.endpoints[endpoint].onjob = false;
                this.loop();
            }, this.endpoints[endpoint].retryAfter + 100);

        } else if(this.limited) {
            
            setTimeout(() => {
                this.limited = false;
                this.loop();
            }, this.retryAfter + 100);

        } else {
            router.emit("done", resp.body);
            this.endpoints[endpoint].onjob = false;
            this.loop();
            
        }
    }
}