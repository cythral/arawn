
module.exports = class Channel {
    constructor({ data, mgr, guild }) {
        for(let key in data) this[key] = data[key];
        this.mgr = mgr;
        this.guild = guild;
    }
    
    message(message) {
        this.mgr.queue.add({
           endpoint : "https://discordapp.com/api/channels/"+this.id+"/messages",
           data : { content : message },
           method : "POST"
        });
    }


    delete(message) {
        this.mgr.queue.add({
            endpoint : "https://discordapp.com/api/channels/"+this.id+"/messages/"+message,
            method : "DELETE"
        });
    }

    async clear(user = null, maxdepth = 10) {
        // get channel messages
        var msgs = [];
        var last = false;
        var depth = 0;

        while(last !== null) {
            depth++;
            let data = { limit : 100 };
            if(last !== false) data.before = last;

            let messagearr = await this.mgr.queue.add({
                endpoint : "https://discordapp.com/api/channels/"+this.id+"/messages",
                method : "GET",
                qs : data
            });

            if(!Array.isArray(messagearr) || messagearr.length === 0) last = null;
            else {
                for(let msg of messagearr) {
                    if(user !== null) {
                        if(msg.author.id === user) {
                            this.delete(msg.id);
                        }
                    } else {
                        this.delete(msg.id);
                    }
                    
                    last = msg.id;
                }
            }
        }
    }
};