const 
    Perms = require('../../permissions.json');


module.exports = class Member {
	constructor({ data, guild, mgr }) {
		for(let key in data) this[key] = data[key];
		this.mgr = mgr;
		this.guild = guild;
	}

	hasPerm(perm, channel = null) {
		let hasperm = false;

		// compute role permissions
		for(let role of Object.keys(this.guild.roles)) {
			role = this.guild.roles[role];
			if(role.name == "@everyone" || this.roles.includes(role.id)) {
				if(role.permissions & Perms.ADMINISTRATOR) return true;
				if(role.permissions & perm) hasperm = true;
			}
		}

		// compute channel permissions
		if(channel !== null) {
			var overwrites = this.guild.channels[channel].permission_overwrites;
			var deny = false, allow = false;

			for(let role of overwrites) {
				if(this.roles.includes(role.id) || this.guild.roles[role.id].name === "@everyone") {
					if(role.deny & perm) deny = true;
					if(role.allow & perm) allow = true;
				}
			}

			if(deny && allow) return true;
			if(deny && !allow) return false;
			if(allow && !hasperm) return true;
		}

		return hasperm;
	}
}